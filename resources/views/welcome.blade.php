@extends('layouts.app')

@section('content')
<div class="container mt-3">
    <h1 class="text-center">Indicadores</h1>
    <hr>
    <div class="row justify-content-center">
        <div class="col-sm-3">
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ route('indicadors.index') }}">Administrar</a></li>
                <li class="list-group-item"><a href="{{ route('tendencias.index') }}">Tendencias</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection