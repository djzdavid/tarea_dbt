@extends('layouts.app')

@push('head')
<!-- datatables -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
@endpush

@section('content')
<div class="container mt-3">
    <h1>Indicadores</h1>
    <hr>
    <div class="row">
        <div class="col-6">
            <a class="btn btn-secondary" href="/" role="button">Volver</a>
        </div>
        <div class="col-6">
            <a class="btn btn-primary float-right" href="{{ route('indicadors.create') }}" role="button">Agregar</a>
        </div>
    </div>
    <hr>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Código</th>
                <th>Unidad Med.</th>
                <th>Valor</th>
                <th>Fecha</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($indicadores as $indicador)
            <tr>
                <th scope="row"><a href="{{ route('indicadors.edit',$indicador->id) }}">{{ $indicador->id }}</a></th>
                <td>{{ $indicador->nombreIndicador }}</td>
                <td>{{ $indicador->codigoIndicador }}</td>
                <td>{{ $indicador->unidadMedidaIndicador }}</td>
                <td>{{ $indicador->valorIndicador }}</td>
                <td>{{ $indicador->fechaIndicador }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Código</th>
                <th>Unidad Med.</th>
                <th>Valor</th>
                <th>Fecha</th>
            </tr>
        </tfoot>
    </table>
</div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>

<script>
    $(function() {
        $('#example').DataTable({
            order: [
                [0, 'asc']
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "infoEmpty": "Mostrando 0 to 0 of 0 registros",
                "infoFiltered": "(Filtrado de _MAX_ total registros)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,

        });
    });
</script>



@endpush