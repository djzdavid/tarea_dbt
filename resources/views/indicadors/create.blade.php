@extends('layouts.app')

@section('content')
<div class="container mt-3">
    <h1>Agregar indicador</h1>
    <hr>
    <div class="row">
        <div class="col-12">
            <a class="btn btn-secondary" href="{{ url()->previous() }}" role="button">Volver</a>
        </div>
    </div>
    <hr>
    <form action="{{ route('indicadors.store') }}" method="POST">
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="InputNombreIndicador">Nombre</label>
                    <input type="text" class="form-control @error('nombreIndicador') is-invalid @enderror" name="nombreIndicador" value="{{ old('nombreIndicador') }}" autocomplete="off">
                    @error('nombreIndicador')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="InputCodigoIndicador">Código</label>
                    <input type="text" class="form-control @error('codigoIndicador') is-invalid @enderror" name="codigoIndicador" value="{{ old('codigoIndicador') }}" autocomplete="off">
                    @error('codigoIndicador')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="InputUnidadMedidaIndicador">Unidad de medida</label>
                    <input type="text" class="form-control @error('unidadMedidaIndicador') is-invalid @enderror" name="unidadMedidaIndicador" value="{{ old('unidadMedidaIndicador') }}" autocomplete="off">
                    @error('unidadMedidaIndicador')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="InputValorIndicador">Valor</label>
                    <input type="text" class="form-control @error('valorIndicador') is-invalid @enderror" name="valorIndicador" value="{{ old('valorIndicador') }}" autocomplete="off">
                    @error('valorIndicador')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="InputFechaIndicador">Fecha</label>
                    <input type="text" class="form-control @error('fechaIndicador') is-invalid @enderror" name="fechaIndicador" value="{{ old('fechaIndicador') }}" autocomplete="off" placeholder="2000/10/01">
                    @error('fechaIndicador')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
    </form>
</div>

</div>
@endsection