@extends('layouts.app')

@section('content')
<div class="container mt-3">
    <h1>Editar indicador</h1>
    <hr>
    <div class="row">
        <div class="col-12">
            <a class="btn btn-secondary" href="{{ url()->previous() }}" role="button">Volver</a>
        </div>
    </div>
    <hr>
    <form action="{{ route('indicadors.update', $indicador->id) }}" method="POST">
        @csrf
        @method('put')
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="InputNombreIndicador">Nombre</label>
                    <input type="text" class="form-control @error('nombreIndicador') is-invalid @enderror" name="nombreIndicador" value="{{ $indicador->nombreIndicador }}" autocomplete="off">
                    @error('nombreIndicador')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="InputCodigoIndicador">Código</label>
                    <input type="text" class="form-control @error('codigoIndicador') is-invalid @enderror" name="codigoIndicador" value="{{ $indicador->codigoIndicador }}" autocomplete="off">
                    @error('codigoIndicador')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="InputUnidadMedidaIndicador">Unidad de medida</label>
                    <input type="text" class="form-control @error('unidadMedidaIndicador') is-invalid @enderror" name="unidadMedidaIndicador" value="{{ $indicador->unidadMedidaIndicador }}" autocomplete="off">
                    @error('unidadMedidaIndicador')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="InputValorIndicador">Valor</label>
                    <input type="text" class="form-control @error('valorIndicador') is-invalid @enderror" name="valorIndicador" value="{{ $indicador->valorIndicador }}" autocomplete="off">
                    @error('valorIndicador')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="InputFechaIndicador">Fecha</label>
                    <input type="text" class="form-control @error('fechaIndicador') is-invalid @enderror" name="fechaIndicador" value="{{ $indicador->fechaIndicador }}" autocomplete="off" placeholder="01/01/2000">
                    @error('fechaIndicador')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Guardar</button>
            <hr>
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">Eliminar</button>

        </div>
    </form>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Eliminar ({{ $indicador->nombreIndicador }})</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ¿Estás seguro de eliminar el indicador?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <form action="{{ route('indicadors.destroy',$indicador->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection