@extends('layouts.app')

@push('head')

@endpush

@section('content')
<div class="container mt-3">
    <h1>Tendencias</h1>
    <hr>
    <a class="btn btn-secondary" href="{{ url()->previous() }}" role="button">Volver</a>
    <hr>
    <div id='graphic'></div>
</div>
@endsection

@push('scripts')
<script src="https://code.highcharts.com/highcharts.js"></script>

<script>
    var data = JSON.parse('<?php echo $valores; ?>');
    const chart = Highcharts.chart('graphic', {
        title: {
            text: '{{ $codigo }}'
        },
        xAxis: {
            type: 'datetime',
            categories: data.map(row => row.y)
        },
        yAxis: {
            title: {
                text: 'Valor'
            }
        },
        series: [{
            name: 'Valor',
            data: data.map(row => Number(row.valor)),
        }]
    });
</script>



@endpush