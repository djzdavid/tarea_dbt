@extends('layouts.app')

@push('head')

@endpush

@section('content')
<div class="container mt-3">
    <h1>Tendencias</h1>
    <hr>
    <a class="btn btn-secondary" href="/" role="button">Volver</a>
    <hr>
    <form action="{{ route('tendencias.store') }}" method="POST">
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="InputCodigoIndicador">Código indicador</label>
                    <select class="custom-select form-control" id="codigoIndicador" name="codigoIndicador">
                        @foreach($codigos as $codigo)
                        <option value="{{ $codigo->codigoIndicador }}"> {{ $codigo->codigoIndicador }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="InputFechaDesde">Fecha desde</label>
                    <input type="text" class="form-control @error('fechaDesde') is-invalid @enderror" name="fechaDesde" value="{{ old('fechaDesde') }}" autocomplete="off" placeholder="2000/01/01">
                    @error('fechaDesde')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    <label for="InputFechaDesde">Fecha hasta</label>
                    <input type="text" class="form-control @error('fechaHasta') is-invalid @enderror" name="fechaHasta" value="{{ old('fechaHasta') }}" autocomplete="off" placeholder="2000/12/31">
                    @error('fechaHasta')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Generar grafico</button>
        </div>
    </form>
</div>
@endsection