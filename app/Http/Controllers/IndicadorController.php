<?php

namespace App\Http\Controllers;

use App\Models\Indicador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Brian2694\Toastr\Facades\Toastr;



class IndicadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indicadores = Indicador::all();
        return view('indicadors.index')->with('indicadores', $indicadores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('indicadors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombreIndicador' => 'required|string|max:255',
            'codigoIndicador' => 'required|string|max:255',
            'unidadMedidaIndicador' => 'required|string|max:255',
            'valorIndicador' => 'required|numeric|max:2147483647',
            'fechaIndicador' => 'required|string|max:255',
        ], [
            'nombreIndicador.required' => 'Este campo es obligatorio.',
            'nombreIndicador.string' => 'Debes ingresar solo letras.',
            'nombreIndicador.max' => 'Solo se permiten 255 caracteres.',
            'codigoIndicador.required' => 'Este campo es obligatorio.',
            'codigoIndicador.string' => 'Debes ingresar solo letras.',
            'codigoIndicador.max' => 'Solo se permiten 255 caracteres.',
            'unidadMedidaIndicador.required' => 'Este campo es obligatorio.',
            'unidadMedidaIndicador.string' => 'Debes ingresar solo letras.',
            'unidadMedidaIndicador.max' => 'Solo se permiten 255 caracteres.',
            'valorIndicador.required' => 'Este campo es obligatorio.',
            'valorIndicador.integer' => 'Debes ingresar solo números.',
            'valorIndicador.max' => 'Solo se permite menor a 2147483647.',
            'fechaIndicador.required' => 'Este campo es obligatorio.',
            'fechaIndicador.string' => 'Debes ingresar solo letras.',
            'fechaIndicador.max' => 'Solo se permiten 255 caracteres.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {
            Indicador::create([
                'nombreIndicador' => $request->nombreIndicador,
                'codigoIndicador' => $request->codigoIndicador,
                'unidadMedidaIndicador' => $request->unidadMedidaIndicador,
                'valorIndicador' => $request->valorIndicador,
                'fechaIndicador' => $request->fechaIndicador,
            ]);

            Toastr::success('Indicador creado correctamente');
            return redirect('indicadors');
        } catch (\Throwable $e) {
            report($e);
            dd($e);
            return false;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Indicador  $indicador
     * @return \Illuminate\Http\Response
     */
    public function show(Indicador $indicador)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Indicador  $indicador
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $indicador = Indicador::find($id);
        return view('indicadors.edit')->with('indicador', $indicador);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Indicador  $indicador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nombreIndicador' => 'required|string|max:255',
            'codigoIndicador' => 'required|string|max:255',
            'unidadMedidaIndicador' => 'required|string|max:255',
            'valorIndicador' => 'required|numeric|max:2147483647',
            'fechaIndicador' => 'required|string|max:255',
        ], [
            'nombreIndicador.required' => 'Este campo es obligatorio.',
            'nombreIndicador.string' => 'Debes ingresar solo letras.',
            'nombreIndicador.max' => 'Solo se permiten 255 caracteres.',
            'codigoIndicador.required' => 'Este campo es obligatorio.',
            'codigoIndicador.string' => 'Debes ingresar solo letras.',
            'codigoIndicador.max' => 'Solo se permiten 255 caracteres.',
            'unidadMedidaIndicador.required' => 'Este campo es obligatorio.',
            'unidadMedidaIndicador.string' => 'Debes ingresar solo letras.',
            'unidadMedidaIndicador.max' => 'Solo se permiten 255 caracteres.',
            'valorIndicador.required' => 'Este campo es obligatorio.',
            'valorIndicador.integer' => 'Debes ingresar solo números.',
            'valorIndicador.max' => 'Solo se permite menor a 2147483647.',
            'fechaIndicador.required' => 'Este campo es obligatorio.',
            'fechaIndicador.string' => 'Debes ingresar solo letras.',
            'fechaIndicador.max' => 'Solo se permiten 255 caracteres.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {
            Indicador::where('id', $id)->firstOrFail()->update([
                'nombreIndicador' => $request->nombreIndicador,
                'codigoIndicador' => $request->codigoIndicador,
                'unidadMedidaIndicador' => $request->unidadMedidaIndicador,
                'valorIndicador' => $request->valorIndicador,
                'fechaIndicador' => $request->fechaIndicador,
            ]);

            Toastr::success('Indicador modificado correctamente');
            return redirect('indicadors');
        } catch (\Throwable $e) {
            report($e);
            dd($e);
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Indicador  $indicador
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Indicador::find($id)->delete();
            Toastr::success('Indicador eliminado correctamente');
            return redirect('indicadors');
        } catch (\Throwable $e) {
            report($e);
            dd($e);
            return false;
        }
    }
}
