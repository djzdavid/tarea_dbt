<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndicadorController;
use App\Http\Controllers\TendenciaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('indicadors', IndicadorController::class);
Route::resource('tendencias', TendenciaController::class);